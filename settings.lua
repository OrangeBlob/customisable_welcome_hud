local center_hud = {
    title_pos = { x = 0.5, y = 0.45 },
    elem_pos = { x = 0.5, y = 0.55 },
    timer_pos = { x = 0.5, y = 0.65 },
    alignment = { x = 0.0, y = 0.0 },
}

local topright_hud = {
    title_pos = { x = 0.98, y = 0.05 },
    elem_pos = { x = 0.98, y = 0.15 },
    timer_pos = { x = 0.98, y = 0.25 },
    alignment = { x = -1.0, y = 0.0 },
}


function retrieve_settings()
    local retrieved_settings = {}

    retrieved_settings.hud_title = minetest.settings:get("customisable_welcome_hud.hud_title")
        or "Welcome to the server!"
    retrieved_settings.hud_elem = minetest.settings:get("customisable_welcome_hud.hud_elem")
        or "Welcome, new player! This is the sample text of this mod!"
    retrieved_settings.wait_time = tonumber(minetest.settings:get("customisable_welcome_hud.wait_time"))
        or 10
    retrieved_settings.hud_type = minetest.settings:get("customisable_welcome_hud.hud_type")
        or "1"
    retrieved_settings.hud_color = minetest.settings:get("customisable_welcome_hud.color")
        or "FFA300"

    if retrieved_settings.hud_type == "1" then
        retrieved_settings.hud_pos = center_hud
    elseif retrieved_settings.hud_type == "2" then
        retrieved_settings.hud_pos = topright_hud
    end

    return retrieved_settings
end


function save_settings(hud_title, hud_elem, wait_time, color, hud_type)
    minetest.settings:set("customisable_welcome_hud.hud_title", hud_title)
    minetest.settings:set("customisable_welcome_hud.hud_elem", hud_elem)
    minetest.settings:set("customisable_welcome_hud.wait_time", wait_time)
    minetest.settings:set("customisable_welcome_hud.color", color)
    if hud_type == "Screen center" then
        minetest.settings:set("customisable_welcome_hud.hud_type", "1")
    elseif hud_type == "Top right" then
        minetest.settings:set("customisable_welcome_hud.hud_type", "2")
    end 
end
