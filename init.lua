dofile(minetest.get_modpath(minetest.get_current_modname()) .. "/settings.lua")
dofile(minetest.get_modpath(minetest.get_current_modname()) .. "/hud.lua")


local function show_settings_formspec(pname)
    local settings = retrieve_settings()
    local formspec =
        "formspec_version[6]" ..
        "size[10,7]" ..
        "label[0.3,0.5;Welcome HUD Customisation ]" ..
        "textarea[0.3,1.5;5.7,0.6;hud_title;HUD title;" .. settings.hud_title .. "]" ..
        "textarea[0.3,2.7;5.7,3;hud_elem;HUD details;" .. settings.hud_elem .. "]" ..
        "button_exit[5.1,6;4.6,0.7;save;Save settings]" ..
        "button_exit[0.3,6;4.6,0.7;cancel;Cancel]" ..
        "textarea[6.2,4;3.5,0.6;wait_time;Wait time (s);" .. settings.wait_time .. "]" ..
        "dropdown[6.2,2.7;3.5,0.7;hud_type;Screen center,Top right;" .. settings.hud_type .. ";false]" ..
        "label[6.2,2.5;HUD type]" ..
        "button_exit[6.2,5;3.5,0.7;preview;Preview]" ..
        "textarea[6.2,1.5;3.5,0.6;color;Title color (#);" .. settings.hud_color .. "]"
    minetest.show_formspec(pname, "customisable_welcome_hud:customise", formspec)
end
minetest.register_on_player_receive_fields(function(player, formname, fields)
    if formname == "customisable_welcome_hud:customise" then

        if fields.save or fields.preview then
            save_settings(
                fields.hud_title,
                fields.hud_elem,
                fields.wait_time,
                fields.color,
                fields.hud_type
            )

            if fields.preview then
                show_welcome_hud(player)
                minetest.after(tonumber(fields.wait_time), function()
                    show_settings_formspec(player:get_player_name())
                end)
            end
        end
    end
end)


minetest.register_chatcommand("welcome_hud", {
    description = "Customise welcome HUD",
    privs = { server = true },

    func = function(name)
        show_settings_formspec(name)
    end,
})


minetest.register_on_newplayer(function(player)
    show_welcome_hud(player)
end)

