dofile(minetest.get_modpath(minetest.get_current_modname()) .. "/settings.lua")

function show_welcome_hud(pname)
    local settings = retrieve_settings()

    local text_length = math.max(string.len(settings.hud_title), string.len(settings.hud_elem))
    local scale_x = text_length / 10
    if text_length < 20 then
        scale_x = 2
    end
    local offset_x = 0
    if settings.hud_type == "2" then
        offset_x = 15
    end
    local hud_background = pname:hud_add({
        hud_elem_type = "image",
        position = settings.hud_pos.elem_pos,
        alignment = settings.hud_pos.alignment,
        text = "background.png",
        scale = {x = scale_x, y = 2.5},
        offset = {x = offset_x, y = 0}
    })

    local hud_title = pname:hud_add({
        hud_elem_type = "text",
        position = settings.hud_pos.title_pos,
        alignment = settings.hud_pos.alignment,
        text = settings.hud_title,
        size = { x = 2, y = 2 },
        number = "0x" .. settings.hud_color
    })
    local hud_elem = pname:hud_add({
        hud_elem_type = "text",
        position = settings.hud_pos.elem_pos,
        alignment = settings.hud_pos.alignment,
        text = settings.hud_elem,
        size = { x = 1, y = 1 },
        number = 0xFFFFFF,
    })
    local hud_timer = pname:hud_add({
        hud_elem_type = "text",
        position = settings.hud_pos.timer_pos,
        alignment = settings.hud_pos.alignment,
        text = "Closing after " .. settings.wait_time .. " seconds",
        size = { x = 1, y = 1 },
        number = 0xd2d2d2,
    })

    for i = settings.wait_time, 1, -1 do
        minetest.after(i, function()
            pname:hud_change(hud_timer, "text", "Closing after " ..
                (settings.wait_time - i) .. " seconds")
        end)
    end

    minetest.after(settings.wait_time, function()
        pname:hud_remove(hud_background)
        pname:hud_remove(hud_title)
        pname:hud_remove(hud_elem)
        pname:hud_remove(hud_timer)
    end)
end
